package com.djamware.TNPWProjekt.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ask")
public class Ask {
    @Id
    private String id;

    @Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
    private String nadpis;

    private String text;
    
    private String email;

    @DBRef
    private User user;
// Default constructor
public Ask() {
}
    public Ask(String nadpis, String text, String email) {
        this.nadpis = nadpis;
        this.text = text;
        this.email = email;
    }
    public Ask(String nadpis, String text) {
        this.nadpis = nadpis;
        this.text = text;
    }
    public String getEmail() {
        return email;
    }

    // Setter method for email field (if needed)
    public void setEmail(String email) {
        this.email = email;
    }
    // Gettery a Settery
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNadpis() {
        return nadpis;
    }

    public void setNadpis(String nadpis) {
        this.nadpis = nadpis;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
