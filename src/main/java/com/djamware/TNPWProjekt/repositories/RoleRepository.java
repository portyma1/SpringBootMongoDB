
package com.djamware.TNPWProjekt.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.djamware.TNPWProjekt.domain.Role;


public interface RoleRepository extends MongoRepository<Role, String> {
    
    Role findByRole(String role);
    
}
