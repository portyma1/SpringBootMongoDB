
package com.djamware.TNPWProjekt.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.djamware.TNPWProjekt.domain.Ask;



public interface AskRepository extends MongoRepository<Ask, String> {
    
    List<Ask> findAll();
    Ask findByEmail(String email);

    Ask findByNadpis(String nazev);
    List<Ask> findAllByOrderByEmailAsc();

    List<Ask> findAllByEmail(String email);
}
