package com.djamware.TNPWProjekt.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.djamware.TNPWProjekt.domain.Offer;

import java.util.List;

public interface OfferRepository extends MongoRepository<Offer, String> {
    List<Offer> findAll();
    Offer findByEmail(String email);
    List<Offer> findAllByOrderByNadpisAsc();
    Offer findByNadpis(String nazev);
    //
    List<Offer> findAllByOrderByEmailAsc();


    List<Offer> findAllByEmail(String email);
}
