
package com.djamware.TNPWProjekt.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.djamware.TNPWProjekt.domain.User;



public interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(String email);
}
