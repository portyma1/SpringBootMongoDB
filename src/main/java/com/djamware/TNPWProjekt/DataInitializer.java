package com.djamware.TNPWProjekt;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.djamware.TNPWProjekt.domain.Ask;
import com.djamware.TNPWProjekt.domain.Offer;

import com.djamware.TNPWProjekt.domain.User;
import com.djamware.TNPWProjekt.repositories.AskRepository;
import com.djamware.TNPWProjekt.repositories.OfferRepository;

import com.djamware.TNPWProjekt.repositories.UserRepository;
import com.djamware.TNPWProjekt.services.CustomUserDetailsService;

@Component
public class DataInitializer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final AskRepository askRepository;
    private final OfferRepository offerRepository;
    @Autowired
    private CustomUserDetailsService userService;
    public DataInitializer(UserRepository userRepository, AskRepository askRepository, OfferRepository offerRepository) {
        this.userRepository = userRepository;
        this.askRepository = askRepository;
        this.offerRepository = offerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        initializeUsers();
        initializeAsks();
        initializeOffers();
    }
    private void initializeUser(String fullname, String email, String password) {
        User user = new User();
        user.setFullname(fullname);
        user.setEmail(email);
        user.setPassword(password);
        userService.saveUser(user);
    }
    private void initializeUsers() {

        if (userRepository.findByEmail("josef.novak@email.com") == null) {
            initializeUser("Josef Novák", "josef.novak@email.com", "password");
        }

        if (userRepository.findByEmail("marie.svobodova@email.com") == null) {
            initializeUser("Marie Svobodová", "marie.svobodova@email.com", "password");
        }

        if (userRepository.findByEmail("petr.kovar@email.com") == null) {
            initializeUser("Petr Kovář", "petr.kovar@email.com", "password");
        }

        if (userRepository.findByEmail("anna.novotna@email.com") == null) {
            initializeUser("Anna Novotná", "anna.novotna@email.com", "password");
        }
       

        if (userRepository.findByEmail("martin.kolar@email.com") == null) {
            initializeUser("Martin Kolař", "martin.kolar@email.com", "password");
        }
    }

    private void initializeAsks() {
        if (askRepository.findByNadpis("Přeprava nábytku") == null) {
            Ask ask1 = new Ask("Přeprava nábytku", "Potřebuji přepravit nábytek do nového bytu.", "josef.novak@email.com");
            askRepository.save(ask1);
        }
        if (askRepository.findByNadpis("Potřebuji pomoc s opravou auta") == null) {
            Ask ask1 = new Ask("Potřebuji pomoc s opravou auta", "Moje auto se porouchalo a potřebuji někoho, kdo by mi pomohl s opravou.", "josef.novak@email.com");
            askRepository.save(ask1);
        }
        if (askRepository.findByNadpis("Úklid po stěhování") == null) {
            Ask ask2 = new Ask("Úklid po stěhování", "Potřebuji někoho, kdo by uklidil po stěhování mého domu.", "anna.novotna@email.com");
            askRepository.save(ask2);
        }
    
        if (askRepository.findByNadpis("Práce v zahradě") == null) {
            Ask ask3 = new Ask("Práce v zahradě", "Hledám pomocníka na úpravu zahrady.", "petr.kovar@email.com");
            askRepository.save(ask3);
        }
    
        if (askRepository.findByNadpis("Doučování matematiky") == null) {
            Ask ask4 = new Ask("Doučování matematiky", "Hledám doučovatele na matematiku pro středoškoláka.", "martin.kolar@email.com");
            askRepository.save(ask4);
        }
        if (askRepository.findByNadpis("Překlady textů") == null) {
            Ask ask6 = new Ask("Překlady textů", "Potřebuji přeložit několik textů z angličtiny do češtiny.", "marie.svobodova@email.com");
            askRepository.save(ask6);
        }
    
        if (askRepository.findByNadpis("Úprava účesu") == null) {
            Ask ask7 = new Ask("Úprava účesu", "Hledám zkušeného kadeřníka na úpravu účesu na svatbu.", "anna.novotna@email.com");
            askRepository.save(ask7);
        }
    
        if (askRepository.findByNadpis("Domácí cvičení") == null) {
            Ask ask8 = new Ask("Domácí cvičení", "Hledám instruktora na domácí cvičení yoga/pilates.", "petr.kovar@email.com");
            askRepository.save(ask8);
        }
    
        if (askRepository.findByNadpis("Návrh interiéru") == null) {
            Ask ask9 = new Ask("Návrh interiéru", "Hledám designéra interiéru na návrh nového bytu.", "martin.kolar@email.com");
            askRepository.save(ask9);
        }
    
        if (askRepository.findByNadpis("Hudební lekce") == null) {
            Ask ask10 = new Ask("Hudební lekce", "Hledám lektora na hudební výuku pro začátečníky.", "marie.svobodova@email.com");
            askRepository.save(ask10);
        }
    }

    private void initializeOffers() {
        if (offerRepository.findByNadpis("Přeprava zásilek") == null) {
            Offer offer1 = new Offer("Přeprava zásilek", "Nabízím přepravu zásilek po celém Česku.", "marie.svobodova@email.com");
            offerRepository.save(offer1);
        }
        
        if (offerRepository.findByNadpis("Přeprava zásilek výpomoc") == null) {
            Offer offer1 = new Offer("Přeprava zásilek výpomoc", "Nabízím přepravu zásilek po celémpřepravu zásilek po celém Česku", "marie.svobodova@email.com");
            offerRepository.save(offer1);
        }
        if (offerRepository.findByNadpis("Stěhování pomoc") == null) {
            Offer offer2 = new Offer("Stěhování pomoc", "Poskytuji pomoc při stěhování, včetně balení a vykládání.", "martin.kolar@email.com");
            offerRepository.save(offer2);
        }
        
        if (offerRepository.findByNadpis("Oprava aut") == null) {
            Offer offer3 = new Offer("Oprava aut", "Provádím opravy a údržbu automobilů za přijatelné ceny.", "petr.kovar@email.com");
            offerRepository.save(offer3);
        }
        if (offerRepository.findByNadpis("Úklidové služby") == null) {
            Offer offer4 = new Offer("Úklidové služby", "Nabízím profesionální úklidové služby pro domácnosti i firmy.", "anna.novotna@email.com");
            offerRepository.save(offer4);
        }
    
        if (offerRepository.findByNadpis("Doručování potravin") == null) {
            Offer offer5 = new Offer("Doručování potravin", "Zajišťuji doručování čerstvých potravin až k vašim dveřím.", "marie.svobodova@email.com");
            offerRepository.save(offer5);
        }
        if (offerRepository.findByNadpis("Malířské práce") == null) {
            Offer offer6 = new Offer("Malířské práce", "Provádím malířské práce v interiérech i exteriérech.", "josef.novak@email.com");
            offerRepository.save(offer6);
        }
    
        if (offerRepository.findByNadpis("Zahradní úpravy") == null) {
            Offer offer7 = new Offer("Zahradní úpravy", "Nabízím kompletní zahradní úpravy a údržbu.", "petr.kovar@email.com");
            offerRepository.save(offer7);
        }
    
        if (offerRepository.findByNadpis("Úprava oblečení") == null) {
            Offer offer8 = new Offer("Úprava oblečení", "Poskytuji služby úprav a oprav oděvů.", "anna.novotna@email.com");
            offerRepository.save(offer8);
        }
    
        if (offerRepository.findByNadpis("Fotografické služby") == null) {
            Offer offer9 = new Offer("Fotografické služby", "Nabízím profesionální fotografické služby pro různé události.", "marie.svobodova@email.com");
            offerRepository.save(offer9);
        }
    
        if (offerRepository.findByNadpis("Kurýrní služby") == null) {
            Offer offer10 = new Offer("Kurýrní služby", "Zajišťuji rychlé a spolehlivé doručení zásilek a dokumentů.", "martin.kolar@email.com");
            offerRepository.save(offer10);
        }
        // Vytvořit a uložit další nabídky, pokud je to potřeba
    }
}
