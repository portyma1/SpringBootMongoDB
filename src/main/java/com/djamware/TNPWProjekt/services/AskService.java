package com.djamware.TNPWProjekt.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.djamware.TNPWProjekt.domain.Ask;
import com.djamware.TNPWProjekt.repositories.AskRepository;

@Service
public class AskService {

     @Autowired
    private AskRepository askRepository;

    public void save(Ask ask) {
        askRepository.save(ask);
    }

    public List<Ask> getAllAsks() {
        return askRepository.findAll();
    }

    public List<Ask> getAllByEmail() {
        return askRepository.findAllByOrderByEmailAsc();
    }

    public List<Ask> getAllAsksByEmail(String email) {
        return askRepository.findAllByEmail(email);
    }

    public void deleteAskById(String id) {
        askRepository.deleteById(id);
    }

    public Ask getAskById(String id) {
        Optional<Ask> askOptional = askRepository.findById(id);
        return askOptional.orElse(null);
    }
    public void deleteAsksByEmail(String email) {
        List<Ask> asksToDelete = askRepository.findAllByEmail(email);
        askRepository.deleteAll(asksToDelete);
    }
}
