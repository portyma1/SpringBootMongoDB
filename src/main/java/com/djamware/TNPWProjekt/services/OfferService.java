package com.djamware.TNPWProjekt.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.djamware.TNPWProjekt.domain.Offer;
import com.djamware.TNPWProjekt.repositories.OfferRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class OfferService {

    @Autowired
    private OfferRepository offerRepository;

    private static final Logger logger = LoggerFactory.getLogger(OfferService.class);

    public void save(Offer offer) {
        offerRepository.save(offer);
    }

    // Method to retrieve all offers
    public List<Offer> getAllOffers() {
        List<Offer> offers = offerRepository.findAll();
        logger.info("Total number of offers retrieved: {}", offers.size());
        return offerRepository.findAll();
    }

    public List<Offer> getAllByNadpis() {
        List<Offer> offers = offerRepository.findAllByOrderByNadpisAsc();
        logger.info("Total number of offers retrieved: {}", offers.size());
        return offers;
    }
    //
    public List<Offer> getAllByEmail() {
        return offerRepository.findAllByOrderByEmailAsc();
    }

    public List<Offer> getAllOffersByEmail(String email) {
        return offerRepository.findAllByEmail(email);
    }
    public void deleteOfferById(String id) {
        offerRepository.deleteById(id);
    }

    public Offer getOfferById(String id) {
        Optional<Offer> askOptional = offerRepository.findById(id);
        return askOptional.orElse(null);
    }
    public void deleteOffersByEmail(String email) {
        List<Offer> offersToDelete = offerRepository.findAllByEmail(email);
        offerRepository.deleteAll(offersToDelete);
    }
}
