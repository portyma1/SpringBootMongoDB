
package com.djamware.TNPWProjekt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.djamware.TNPWProjekt.domain.Ask;
import com.djamware.TNPWProjekt.domain.Offer;
import com.djamware.TNPWProjekt.domain.User;
import com.djamware.TNPWProjekt.services.AskService;
import com.djamware.TNPWProjekt.services.CustomUserDetailsService;
import com.djamware.TNPWProjekt.services.OfferService;


@Controller
public class AuthController {

    @Autowired
    private CustomUserDetailsService userService;
    @Autowired
    private AskService askService;

    @Autowired
    private OfferService offerService; 
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView signup() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("signup");
        return modelAndView;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView createNewUser(User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "Tato přezdívka už je obsazená");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("signup");
        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "Registrace proběha úspěšně");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("login");

        }
        return modelAndView;
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
public ModelAndView dashboard() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    modelAndView.addObject("currentUser", user);
    modelAndView.addObject("fullName", "Vítej zpět " + user.getFullname());
    modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
    
    // Load asks and offers by email of the currently logged-in user
    List<Ask> userAsks = askService.getAllAsksByEmail(user.getEmail());
    modelAndView.addObject("userAsks", userAsks);
    
    List<Offer> userOffers = offerService.getAllOffersByEmail(user.getEmail());
    modelAndView.addObject("userOffers", userOffers);
    
    modelAndView.setViewName("dashboard");
    return modelAndView;
}


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        return modelAndView;
    }
 

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("fullName", "Vítej zpět " + user.getFullname());
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
        modelAndView.setViewName("create");
        return modelAndView;
    }



@RequestMapping(value = "/ask", method = RequestMethod.GET)
public ModelAndView ask() {
    ModelAndView modelAndView = new ModelAndView();


    // Add CSS file to the model
    modelAndView.addObject("cssFile", "style.css");

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    modelAndView.addObject("currentUser", user);
    modelAndView.addObject("fullName", "Vítej zpět " + user.getFullname());
    modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");

    // Retrieve asks from the database
    List<Ask> asks = askService.getAllAsks();
    modelAndView.addObject("asks", asks); // Add asks to the model

    modelAndView.setViewName("ask");
    return modelAndView;
}









    @RequestMapping(value = "/offer", method = RequestMethod.GET)
    public ModelAndView offer() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("fullName", "Vítej zpět " + user.getFullname());
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");

        // Retrieve offers from the database
        List<Offer> offers = offerService.getAllOffers();
        modelAndView.addObject("offers", offers); // Add offers to the model

        modelAndView.setViewName("offer");
        return modelAndView;
    }
    
}
