package com.djamware.TNPWProjekt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.djamware.TNPWProjekt.domain.Ask;
import com.djamware.TNPWProjekt.domain.Offer;
import com.djamware.TNPWProjekt.services.AskService;
import com.djamware.TNPWProjekt.services.CustomUserDetailsService;
import com.djamware.TNPWProjekt.services.OfferService;


@Controller
public class FormController {
    @Autowired
    private AskService askService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private CustomUserDetailsService userService;

    @PostMapping("/create")
    public String create(@RequestParam("title") String title,
                         @RequestParam("text") String text,
                         @RequestParam("type") String type
                        ) {

                            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        if ("ask".equals(type)) {
            Ask ask = new Ask(title, text, loggedInUserEmail); // Constructor modified to accept email
            askService.save(ask);
        } if ("offer".equals(type)) {
            Offer offer = new Offer(title, text, loggedInUserEmail); // Constructor modified to accept email
            offerService.save(offer);
        }
        return "redirect:/dashboard"; // Redirect to dashboard after form submission
    }

    @DeleteMapping("/deleteAsk/{askId}")
    public String deleteAsk(@PathVariable("askId") String askId) {
        // Call the service to delete the offer by its ID
        askService.deleteAskById(askId);
        // Redirect back to the dashboard after deletion
        return "redirect:/dashboard";
    }
    
    // Combined method for handling both GET and POST requests for editing an ask
    @RequestMapping(value = "/deleteAsk", method = RequestMethod.GET)
    public ModelAndView showDeleteAsk(@RequestParam(value = "id", required = false) String askId) {
        ModelAndView modelAndView = new ModelAndView();
        if (askId != null) {
            // If askId is provided, retrieve the ask by its ID and populate the form fields
            Ask ask = askService.getAskById(askId);
            modelAndView.addObject("askId", askId);
    
        }
        modelAndView.setViewName("deleteAsk");
        return modelAndView;
    }
    


    @DeleteMapping("/deleteOffer/{offerId}")
public String deleteOffer(@PathVariable("offerId") String offerId) {
    // Call the service to delete the offer by its ID
    offerService.deleteOfferById(offerId);
    // Redirect back to the dashboard after deletion
    return "redirect:/dashboard";
}

// Combined method for handling both GET and POST requests for editing an ask
@RequestMapping(value = "/deleteOffer", method = RequestMethod.GET)
public ModelAndView showDeleteOffer(@RequestParam(value = "id", required = false) String offerId) {
    ModelAndView modelAndView = new ModelAndView();
    if (offerId != null) {
        // If askId is provided, retrieve the ask by its ID and populate the form fields
        Offer offer = offerService.getOfferById(offerId);
        modelAndView.addObject("offerId", offerId);

    }
    modelAndView.setViewName("deleteOffer");
    return modelAndView;
}






    @DeleteMapping("/deleteUser/{userId}") // Add this line
    public String deleteUser() {
        // Get the logged-in user's email
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            // Delete all asks associated with the logged-in user's email
        askService.deleteAsksByEmail(loggedInUserEmail);

        // Delete all offers associated with the logged-in user's email
        offerService.deleteOffersByEmail(loggedInUserEmail);
        // Delete the user by email
        userService.deleteUserByEmail( loggedInUserEmail);
        
        // Redirect to a confirmation page or any other appropriate URL
        return "redirect:/";
    }
// Combined method for handling both GET and POST requests for deleting a user
@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
public ModelAndView deleteUserForm(@RequestParam(value = "id", required = false) String userId) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("userId", userId);
    modelAndView.setViewName("deleteUser"); // Set the view name
    return modelAndView;
}



// Combined method for handling both GET and POST requests for editing an ask
@RequestMapping(value = "/editAsk", method = RequestMethod.GET)
public ModelAndView showEditAskForm(@RequestParam(value = "id", required = false) String askId) {
    ModelAndView modelAndView = new ModelAndView();
    if (askId != null) {
        // If askId is provided, retrieve the ask by its ID and populate the form fields
        Ask ask = askService.getAskById(askId);
        modelAndView.addObject("askId", askId);
        modelAndView.addObject("askTitle", ask.getNadpis());
        modelAndView.addObject("askText", ask.getText());
    }
    modelAndView.setViewName("editAsk");
    return modelAndView;
}



@PutMapping("/editAsk/{askId}")
public String editAsk(
        @RequestParam("askId") String askId,
        @RequestParam("title") String title,
        @RequestParam("text") String text
) {
    // Retrieve the ask by its ID
    Ask ask = askService.getAskById(askId);
    // Update the ask with the new title and text
    ask.setNadpis(title);
    ask.setText(text);
    // Save the updated ask
    askService.save(ask);
    // Redirect back to the dashboard
    return "redirect:/dashboard";
}


@RequestMapping(value = "/editOffer", method = RequestMethod.GET)
public ModelAndView showEditOfferForm(@RequestParam(value = "id", required = false) String offerId) {
    ModelAndView modelAndView = new ModelAndView();
    if (offerId != null) {
        // If offerId is provided, retrieve the offer by its ID and populate the form fields
        Offer offer = offerService.getOfferById(offerId);
        modelAndView.addObject("offerId", offerId);
        modelAndView.addObject("offerTitle", offer.getNadpis());
        modelAndView.addObject("offerText", offer.getText());
    }
    modelAndView.setViewName("editOffer");
    return modelAndView;
}




@PutMapping("/editOffer/{offerId}")
public String editOffer(
        @PathVariable("offerId") String offerId,
        @RequestParam("title") String title,
        @RequestParam("text") String text
) {
    // Retrieve the offer by its ID
    Offer offer = offerService.getOfferById(offerId);
    // Update the offer with the new title and text
    offer.setNadpis(title);
    offer.setText(text);
    // Save the updated offer
    offerService.save(offer);
    // Redirect back to the dashboard
    return "redirect:/dashboard";
}


}
