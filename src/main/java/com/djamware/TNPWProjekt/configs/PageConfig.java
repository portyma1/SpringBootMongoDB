
package com.djamware.TNPWProjekt.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class PageConfig implements WebMvcConfigurer {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/dashboard").setViewName("dashboard");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/offer").setViewName("offer");
        registry.addViewController("/ask").setViewName("ask");
        registry.addViewController("/create").setViewName("create");
        registry.addViewController("/editAsk").setViewName("editAsk");
        registry.addViewController("/editOffer").setViewName("editOffer");
        registry.addViewController("/deleteOffer").setViewName("deleteOffer");
        registry.addViewController("/deleteUser").setViewName("deleteUser");
    }

}
